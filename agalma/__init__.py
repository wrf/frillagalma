# Agalma - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2014 Brown University. All rights reserved.
#
# This file is part of Agalma.
#
# Agalma is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Agalma is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Agalma.  If not, see <http://www.gnu.org/licenses/>.

"""
Agalma is an automated tool that constructs matrices for
phylogenomic analyses, allowing complex phylogenomic analyses to be
implemented and described unambiguously as a series of high-level
commands.  The user provides raw Illumina transcriptome data, and
Agalma produces annotated assemblies, aligned gene sequence matrices,
a preliminary phylogeny, and detailed diagnostics that allow the
investigator to make extensive assessments of intermediate analysis
steps and the final results.  Sequences from other sources, such as
externally assembled genomes and transcriptomes, can also be
incorporated in the analyses. Agalma is built on the BioLite
bioinformatics framework, which tracks provenance, profiles processor
and memory use, records diagnostics, manages metadata, installs
dependencies, logs version numbers and calls to external programs, and
enables rich HTML reports for all stages of the analysis. Agalma
includes a small test data set and a built-in test analysis of these
data.

See `__cite__` for information on citing Agalma.
"""

__cite__ = """Please cite Agalma as:

Dunn CW, Howison M, Zapata F. 2013. Agalma: an automated phylogenomics
workflow. BMC Bioinformatics 14(1): 330. doi:10.1186/1471-2105-14-330

Please cite BioLite as:

Howison M, Sinnott-Armstrong N, Dunn CW. 2012. BioLite, a lightweight
bioinformatics framework with automated tracking of diagnostics and provenance.
In Proceedings of the 4th USENIX Workshop on the Theory and Practice of
Provenance (TaPP '12), 14-15 June 2012, Boston, MA, USA.

The following programs are integral to Agalma and should also be cited:

Andrews S. FastQC: A quality control tool for high throughput sequence data.
[http://www.bioinformatics.bbsrc.ac.uk/projects/fastqc/]

Langmead B, Trapnell C, Pop M, Salzberg SL. 2009. Ultrafast and
memory-efficient alignment of short DNA sequences to the human genome. Genome
Biology. doi:10.1186/gb-2009-10-3-r25

Grabherr MG, Haas BJ, Yassour M, et al. 2011. Full-length transcriptome
assembly from RNA-Seq data without a reference genome. Nat. Biotech.
doi:10.1038/nbt.1883

Altschul SF, Madden TL, Schaffer AA, Zhang J, Zhang Z, Miller W, Lipman DJ.
1997. Gapped BLAST and PSI-BLAST: a new generation of protein database search
programs. Nucl. Acids Res. doi:10.1093/nar/25.17.3389

Dongen Sv and Abreu-Goodger C. 2012. Using MCL to Extract Clusters from
Networks. Methods in Molecular Biology. doi:10.1007/978-1-61779-361-5_15

Ranwez V, Harispe S, Delsuc F, Douzery EJP. 2011. MACSE: Multiple Alignment
of Coding SEquences Accounting for Frameshifts and Stop Codons. PLoS ONE.
doi:10.1371/journal.pone.0022594

Katoh K, Standley DM. 2013. MAFFT Multiple Sequence Alignment Software Version
7: Improvements in Performance and Usability. Mol Biol Evol 30: 772-780.
doi:10.1093/molbev/mst010

Talavera G, Castresana J. 2007. Improvement of phylogenies after removing
divergent and ambiguously aligned blocks from protein sequence alignments.
Systematic Biology. doi:10.1080/10635150701472164

Stamatakis A. 2006. RAxML-VI-HPC: maximum likelihood-based phylogenetic
analyses with thousands of taxa and mixed models. Bioinformatics.
doi:10.1093/bioinformatics/btl446

Tange, O. 2011. GNU Parallel - The Command-Line Power Tool. ;login: The
USENIX Magazine, February 2011:42-47.
"""

__version__ = "0.4.0"

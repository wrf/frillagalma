#!/usr/bin/env python

import os
import sys
from biolite import workflows

fasta = sys.argv[1]
command = 'blastp -db swissprot -evalue 1e-10 -max_target_seqs 1 -outfmt "6 qseqid sseqid stitle pident bitscore evalue"'

print workflows.blast.split_query(
			fasta, command, 100000, fasta.partition('.')[0])


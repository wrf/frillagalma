#!/usr/bin/env python

import sys
from Bio import SeqIO

for record in SeqIO.parse(sys.stdin, "fasta"):
	if "S rRNA" in record.description and len(record.seq) < 5000:
		SeqIO.write(record, sys.stdout, "fasta")

# vim: noexpandtab ts=4 sw=4

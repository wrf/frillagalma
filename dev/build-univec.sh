#!/bin/bash
set -e
wget http://ftp.ncbi.nih.gov/pub/UniVec/UniVec
makeblastdb -dbtype nucl -in UniVec -title "Agalma UniVec build $(date)" -out ../agalma/blastdb/UniVec

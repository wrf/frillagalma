#!/bin/bash

set -e

TESTDATA=$(python -c "import agalma; print agalma.__path__[0]")/testdata
LEVEL=${1-"1"}
DIR=`mktemp -d $PWD/agalma-test-phylogeny-XXX`

echo "Created temp directory '$DIR'"
cd $DIR

if [ -n "$BIOLITE_RESOURCES" ]
then
	SAVE_BIOLITE_RESOURCES=",$BIOLITE_RESOURCES"
fi
export BIOLITE_RESOURCES="database=$DIR/biolite.sqlite,agalma_database=$DIR/agalma.sqlite,outdir=$DIR$SAVE_BIOLITE_RESOURCES"

agalma catalog insert --id "SRX288285" --paths $TESTDATA/SRX288285_1.fq $TESTDATA/SRX288285_2.fq --species "Agalma elegans" --ncbi_id "316166" --library_id "SRR871526" --library_type "TRANSCRIPTOMIC" --sequencer "Illumina HiSeq 2000" --seq_center "Dunnlab" --sample_prep "Trizol | Illumina TruSeq RNA Sample Prep Kit RNA Purification Beads ; 2 rounds | Illumina TruSeq RNA Sample Prep Kit"
agalma catalog insert --id "SRX288432" --paths $TESTDATA/SRX288432_1.fq $TESTDATA/SRX288432_2.fq --species "Craseoa lathetica" --ncbi_id "316205" --library_id "SRR871529" --library_type "TRANSCRIPTOMIC" --sequencer "Illumina HiSeq 2000" --seq_center "Dunnlab" --sample_prep "Invitrogen Dynabeads mRNA TESTDATAECT kit ; 1 round | Illumina TruSeq RNA Sample Prep Kit"
agalma catalog insert --id "SRX288431" --paths $TESTDATA/SRX288431_1.fq $TESTDATA/SRX288431_2.fq --species "Physalia physalis" --ncbi_id "168775" --library_id "SRR871528" --library_type "TRANSCRIPTOMIC" --sequencer "Illumina HiSeq 2000" --seq_center "Dunnlab" --sample_prep "Trizol | Illumina TruSeq RNA Sample Prep Kit RNA Purification Beads ; 2 rounds | Illumina TruSeq RNA Sample Prep Kit"
agalma catalog insert --id "SRX288430" --paths $TESTDATA/SRX288430_1.fq $TESTDATA/SRX288430_2.fq --species "Nanomia bijuga" --ncbi_id "168759" --library_id "SRR871527" --library_type "TRANSCRIPTOMIC" --sequencer "Illumina HiSeq 2000" --seq_center "Dunnlab" --sample_prep "Trizol | Invitrogen Dynabeads mRNA Purification Kit ; 2 rounds | Illumina TruSeq RNA Sample Prep Kit"
agalma catalog insert --id "JGI_NEMVEC" --paths $TESTDATA/JGI_NEMVEC.fa --species "Nematostella vectensis" --ncbi_id "45351" --itis_id "52498" --library_type "genome" --note "Gene predictions from genome sequencing"

cd $DIR

for ID in SRX288285 SRX288430 SRX288431 SRX288432
do
	agalma postassemble --id $ID --assembly $TESTDATA/$ID.fa
	agalma load --id $ID
done

for ID in JGI_NEMVEC
do
	agalma postassemble --id $ID --external
	agalma load --id $ID
done

LOAD_IDS=$(agalma diagnostics list -n load 2>/dev/null | awk 'NR>1 {print $2}')

ID=AllByAllTest

agalma homologize --id $ID $LOAD_IDS

if [ $LEVEL -gt 2 ]; then
agalma multalignx --id $ID
else
agalma multalign --id $ID
fi

agalma genetree --id $ID
agalma treeprune --id $ID

if [ $LEVEL -gt 2 ]; then
agalma multalignx --id $ID
else
agalma multalign --id $ID
fi

if [ $LEVEL -gt 2 ]; then
agalma supermatrix --id $ID --seq_type nucleotide
fi
agalma supermatrix --id $ID
agalma speciestree --id $ID --raxml_flags="-o Nematostella_vectensis"

if [ $LEVEL -gt 1 ]; then
agalma supermatrix --id $ID --proportion 1.0
agalma speciestree --id $ID --raxml_flags="-o Nematostella_vectensis"
fi

agalma report --id $ID --outdir report
agalma resources --id $ID --outdir report
agalma phylogeny_report --id $ID --outdir report

if [ $LEVEL -gt 3 ]; then

ID=OMATest

agalma orthologize --id $ID $LOAD_IDS
agalma multalign --id $ID
agalma supermatrix --id $ID
agalma speciestree --id $ID --raxml_flags="-o Nematostella_vectensis"

agalma report --id $ID --outdir report-oma
agalma resources --id $ID --outdir report-oma
agalma phylogeny_report --id $ID --outdir report-oma

fi

